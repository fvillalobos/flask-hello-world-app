import os
from datetime import datetime

from flask import Flask, render_template

app = Flask(__name__)

date_format = os.environ['DATE_FORMAT']


@app.route('/time')
def return_time():
    return render_template('site.html', text=f'It is currently {datetime.now().strftime(date_format)}')


@app.route('/')
@app.route('/<name>')
def hello_world(name='World'):
    return render_template('site.html', text=f'Hello {name}!')


if __name__ == '__main__':
    app.run()
